/****************************************
 * Mapping emails to people and companies
 ****************************************/
create table if not exists idmap.person(
    personid    integer primary key,
    personname  text not null, /* "Canonical" name for the person.  Might not be unique. */
    persondesc  text           /* Anything useful to distinguish this person from someone else w/ the same name */
);

create table if not exists idmap.companies(
    companyid   integer primary key,
    companyname text not null,
    companydesc text
);

/*
 * This should only be used where the email address reliable indicates employer;
 * e.g., citrix.com -> Citrix, but gmail.com !-> Google.
 */
create table if not exists idmap.hostname_to_company(
    hostname    text not null,
    companyid   integer not null,
    unique(hostname),
    foreign key(companyid) references companies
);

create table if not exists idmap.person_to_company(
    personid  integer not null,
    companyid integer not null,
    startdate date, /* NULL here means 'for as  long as we know' */
    enddate   date, /* NULL here means "currently" */
    unique(personid, companyid, startdate, enddate),
    foreign key(personid) references person,
    foreign key(companyid) references companies
);

create table if not exists idmap.address_to_person(
    mailboxname text not null,
    hostname    text not null,
    personid integer not null,
    primary key(mailboxname, hostname)
    unique(mailboxname, hostname, personid),
    foreign key(personid) references person
);

/*
 * A way to tag multiple email addresses with a specific type; for
 * instance, 'bot' for mail sent by bots, or 'list-linux' for email
 * addresses associated with the Linux kernel
*/
create table if not exists idmap.tags(
    tagid integer primary key,
    tagname text not null,
    tagdesc text
);

create table if not exists idmap.address_to_tag(
    mailboxname text not null,
    hostname    text not null,
    tagid integer not null,
    primary key(mailboxname, hostname)
    unique(mailboxname, hostname, tagid),
    foreign key(tagid) references tags
);
