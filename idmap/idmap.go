// idmap is a package and a database schema to assist in mapping email addresses
// to people, companies or functions (such as "bots" or "mailing lists"), for
// doing analysis on development email lists and git histories.
//
// Problems this solves:
//   - Mapping multiple email addresses (either genuine separate mailboxes or
//     typos) to the same person
//   - "Default" mapping of host names to companies, so that e.g., all "@amd.com"
//     addresses go to AMD
//   - Mapping of people to companies over time, so that as people switch from one
//     company to another, "person@xen.org" ends up mapping to the appropriate
//     company
//   - Mapping email addresses to "tags"; for example, marking messages from CI
//     systems so that we can easily exclude them from analysis, or marking messages
//     which cc (say) the Linux Kernel lists, so that we can analyze messages CC'd
//     or not CC'd to any Linux Kernel list.
//
// The schema can be found in the file [idmap-schemas.sql].  In general, most
// queries / functions assume that the idmap database has been attached using
// SQLite's `ATTACH` command as `idmap`; thus to access the `person` table,
// you'd use `idmap.person`.
//
// idmap uses the `lifecycle` framework to simplify database schema versioning.
// Generally you'll want to attach it to an existing open database using Attach.
// You can also use Open if you want to perform ops on the database itself.
package idmap

import (
	"database/sql"
	_ "embed"
	"fmt"

	"github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/sqlite3"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/martyros/sqlutil/lifecycle"
	"gitlab.com/martyros/sqlutil/txutil"
)

//go:embed idmap-schema.sql
var sqlIdmapCreate string

const schemaCurrent = 1

var (
	dialect = goqu.Dialect("sqlite3")

	SIdmap = goqu.S("idmap")

	// Tables
	TPerson            = SIdmap.Table("person")
	TCompanies         = SIdmap.Table("companies")
	THostnameToCompany = SIdmap.Table("hostname_to_company")
	TPersonToCompany   = SIdmap.Table("person_to_company")
	TAddressToPerson   = SIdmap.Table("address_to_person")
	TTags              = SIdmap.Table("tags")
	TAddressToTag      = SIdmap.Table("address_to_tag")

	// Columns for Person
	CPersonId   = goqu.C("personid")
	CPersonName = goqu.C("personname")
	CPersonDesc = goqu.C("persondesc")

	// Columns for AddressToPerson
	CMailboxName = goqu.C("mailboxname")
	CHostname    = goqu.C("hostname")

	// Columns for Companies
	CCompanyId   = goqu.C("companyid")
	CCompanyName = goqu.C("companyname")
	CCompanyDesc = goqu.C("companydesc")

	// Columns for PersonToCompany
	CStartDate = goqu.C("startdate")
	CEndDate   = goqu.C("enddate")

	// Columns for Tags
	CTagId   = goqu.C("tagid")
	CTagName = goqu.C("tagname")
	CTagDesc = goqu.C("tagdesc")

	// Intermediate table names
	CPersonLabel   = goqu.C("personlabel")
	CCompanyLabel  = goqu.C("companylabel")
	CHostCompany   = goqu.C("hostcompany")
	CPersonCompany = goqu.C("personcompany")

	// A table with all of a person's addresses, and all companies they've
	// worked for (with the company named "personcompanyinner")
	QPersonEmailsCompanies = dialect.From(TAddressToPerson).
				NaturalJoin(TPerson).
				NaturalLeftJoin(TPersonToCompany).
				NaturalLeftJoin(TCompanies).
				Select(CPersonId,
			CPersonName,
			CMailboxName, CHostname,
			CStartDate, CEndDate,
			CCompanyName.As("personcompanyinner"))

	// A table mapping hostname to company names, with companyname as
	// "hostcompany "
	QCompanyEmails = dialect.From(THostnameToCompany).
			NaturalJoin(TCompanies).
			Select(CHostname, CCompanyName.As("hostcompany"))
)

type DbConfig struct {
	Filename      string
	UpgradeSchema bool
}

func attach(eq sqlx.Ext, dbc DbConfig) error {
	if _, err := eq.Exec(fmt.Sprintf(`attach "file:%s" as idmap`, dbc.Filename)); err != nil {
		return err
	}

	lcfg := lifecycle.Schema(schemaCurrent,
		lifecycle.Recipe{Sql: sqlIdmapCreate}).
		SetSchemaName("gitlab.com/xen-project/people/gdunlap/mgstat/idmap").
		SetParamTableName("idmap.attributes").
		SetAutoMigrate(dbc.UpgradeSchema)

	if err := lcfg.OpenTx(eq); err != nil {
		return fmt.Errorf("Opening idmap database: %w", err)
	}

	return nil
}

// Open returns an SQLx db.  Because idmap is primarily meant to be attached as
// a secondary database, it opens an empty "memory" database and attaches idmap
// to it.
func Open(dbc DbConfig) (*sqlx.DB, error) {
	DB, err := sqlx.Open("sqlite3", ":memory:?cache=shared&_foreign_keys=on")
	if err != nil {
		return nil, err
	}

	err = txutil.TxLoopDb(DB, func(eq sqlx.Ext) error {
		return attach(eq, dbc)
	})

	if err != nil {
		return nil, err
	}

	return DB, nil
}

// Attach attaches the idmap database as `idmap` to an existing sqlite3 database
func Attach(eq sqlx.Ext, dbc DbConfig) error {
	return attach(eq, dbc)
}

type Person struct {
	PersonId   int64
	PersonName string
	PersonDesc sql.NullString
}

// AddPerson will insert the person into idmap.person.  PersonId is set if
// successful.
func AddPerson(e sqlx.Execer, p *Person) error {
	res, err := e.Exec(`insert into idmap.person(personname, persondesc) values(?, ?)`, p.PersonName, p.PersonDesc)
	if err != nil {
		return err
	}

	pid, err := res.LastInsertId()
	if err != nil {
		return fmt.Errorf("Getting personid: %v", err)
	}

	p.PersonId = pid

	return nil
}
