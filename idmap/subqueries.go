package idmap

import "github.com/doug-martin/goqu/v9"

// Return a SelectDataset with the addresses matching the given tag
func QueryTagAddress(tags []string) *goqu.SelectDataset {
	in := make([]interface{}, len(tags))
	for i := range tags {
		in[i] = tags[i]
	}

	return dialect.From(TAddressToTag).
		NaturalJoin(TTags).
		Where(CTagName.In(in))
}
