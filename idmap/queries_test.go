package idmap_test

import (
	"testing"

	"gitlab.com/xen-project/people/gdunlap/mgstats/idmap"
)

func TestQueries(t *testing.T) {
	q := idmap.QueryTagAddress([]string{"bot"})

	sql, params, err := q.Prepared(true).ToSQL()
	if err != nil {
		t.Errorf("Error preparing SQL: %v", err)
	}
	t.Logf("SQL: %v\nParams: %v", sql, params)

}
