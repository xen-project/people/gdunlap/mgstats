// Package htmlstat contains functions useful for generating HTML graphs
package htmlstat

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"slices"
)

type SunburstElem struct {
	Name     string         `json:"name"`
	Value    float64        `json:"value,omitempty"`
	Children []SunburstElem `json:"children,omitempty"`
}

// SunburstAddElem will add an element of value value at the specified path,
// returning the new top-level data array (adding the new element of necessary).
func SunburstAddElem(sbdata []SunburstElem, path []string, value float64) []SunburstElem {
	idx := slices.IndexFunc(sbdata, func(se SunburstElem) bool { return se.Name == path[0] })

	if idx < 0 {
		idx = len(sbdata)
		sbdata = append(sbdata, SunburstElem{
			Name: path[0],
		})
	}

	sbe := &sbdata[idx]

	if len(path) == 1 {
		sbe.Value += value
	} else {
		sbe.Children = SunburstAddElem(sbe.Children, path[1:], value)
	}

	return sbdata
}

// SunburstImportData will convert the data series into a sunburst tree, using
// the function provided to convert the data into a path and a value.
//
// Example:
//
//	    data := []struct {
//	   	path  string
//	   	count int
//	    }{
//	   	{"./working.diff", 96},
//	    	{"./go.mod", 36},
//	   		{"xenstats/xenstats-cli/cmd/root.go", 94},
//	    	{"xenstats/xenstats-cli/LICENSE", 21},
//	    	{"xenstats/xenstats-cli/main.go", 28},
//	    	{"htmlstat/sunburst_test.go", 43},
//	   		{"htmlstat/sunburst.go", 50},
//	    	{"mlstats/query_test.go", 60},
//	   		{"mlstats/db.go", 107},
//	    	{"mlstats/annotate_test.go", 55},
//	    }
//	    sbdata, _ := htmlstat.SunburstImportData(
//		    data,
//	    	func(s struct {
//	    		path  string
//		    	count int
//		    }) ([]string, float64, error) {
//		    	out := strings.Split(s.path, "/")
//	    		return out, float64(s.count), nil
//		    })
func SunburstImportData[T any](data []T, f func(T) ([]string, float64, error)) ([]SunburstElem, error) {
	var sbdata []SunburstElem

	for _, t := range data {
		path, value, err := f(t)

		if err != nil {
			return nil, err
		}

		sbdata = SunburstAddElem(sbdata, path, value)
	}

	return sbdata, nil
}

//go:embed sunburst.gtpl
var sunburstTemplateContent string

var SunburstTmpl = template.Must(template.New("huh").Parse(sunburstTemplateContent))

// SunburstBasicPage will generate a basic HTML page with a sunburst chart
// containing the provided data.
func SunburstBasicPage(w io.Writer, data []SunburstElem) error {
	json, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("Marshalling json: %w", err)
	}

	return SunburstTmpl.ExecuteTemplate(w, "sunburst-basic", map[string]any{
		"Data": template.JS(json),
	})
}
