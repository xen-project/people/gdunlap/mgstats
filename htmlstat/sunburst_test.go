package htmlstat_test

import (
	"os"
	"strings"
	"testing"

	"gitlab.com/xen-project/people/gdunlap/mgstats/htmlstat"
)

func TestSunburst(t *testing.T) {
	{
		var data []htmlstat.SunburstElem

		for _, v := range [][]string{
			{"Company A", "Technical", "Team Leaders"},
			{"Company A", "Technical", "Architects"},
			{"Company A", "Technical", "Developers"},
			{"Company A", "Technical", "Testers"},
			{"Company A", "Sales", "Analysts"},
			{"Company A", "Sales", "Executives"},
			{"Company A", "HR"},
			{"Company A", "Management"},
		} {
			data = htmlstat.SunburstAddElem(data, v, 0)
		}

		// b, err := json.MarshalIndent(data, "", " ")
		// if err != nil {
		// 	t.Errorf("Marshaling data: %v", err)
		// } else {
		// 	t.Logf("JSON: %s", string(b))
		// }

		if file, err := os.CreateTemp("", "*.html"); err != nil {
			t.Errorf("Creating temporary file: %v", err)
		} else if err := htmlstat.SunburstBasicPage(file, data); err != nil {
			t.Errorf("Writing basic page: %v", err)
		} else {
			t.Logf("Created company starburst file at %s", file.Name())
		}

	}

	{
		data := []struct {
			path  string
			count int
		}{
			{"mgstats/working.diff", 96},
			{"mgstats/go.mod", 36},
			{"mgstats/xenstats/xenstats-cli/cmd/root.go", 94},
			{"mgstats/xenstats/xenstats-cli/LICENSE", 21},
			{"mgstats/xenstats/xenstats-cli/main.go", 28},
			{"mgstats/htmlstat/sunburst_test.go", 43},
			{"mgstats/htmlstat/sunburst.go", 50},
			{"mgstats/mlstats/query_test.go", 60},
			{"mgstats/mlstats/db.go", 107},
			{"mgstats/mlstats/annotate_test.go", 55},
		}

		sbdata, _ := htmlstat.SunburstImportData(
			data,
			func(s struct {
				path  string
				count int
			}) ([]string, float64, error) {
				out := strings.Split(s.path, "/")
				return out, float64(s.count), nil
			})

		// b, err := json.MarshalIndent(sbdata, "", " ")
		// if err != nil {
		// 	t.Errorf("Marshaling data: %v", err)
		// } else {
		// 	t.Logf("JSON: %s", string(b))
		// }

		if file, err := os.CreateTemp("", "*.html"); err != nil {
			t.Errorf("Creating temporary file: %v", err)
		} else if err := htmlstat.SunburstBasicPage(file, sbdata); err != nil {
			t.Errorf("Writing basic page: %v", err)
		} else {
			t.Logf("Created file starburst file at %s", file.Name())
		}
	}

}
