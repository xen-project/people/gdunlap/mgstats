{{define "sunburst-basic"}}
<html >
<head>
  <meta charset="UTF-8">
  <script src="https://cdn.anychart.com/releases/8.12.0/js/anychart-core.min.js"></script>
  <script src="https://cdn.anychart.com/releases/8.12.0/js/anychart-sunburst.min.js"></script>
  <style type="text/css">
    html,
    body,
    #container {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
    }
  </style>
</head>

<body>
  <div id="container"></div>
</body>

<script>
anychart.onDocumentReady(function () {
    var data = {{.Data}}
    var chart = anychart.sunburst(data, "as-tree");
    chart.container("container");
    chart.calculationMode("parent-independent");
    chart.sort("asc");
    chart.draw();
})
</script>
</html>
{{end}}