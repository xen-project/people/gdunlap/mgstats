package mlstats_test

import (
	"testing"

	"gitlab.com/xen-project/people/gdunlap/mgstats/mlstats"
)

func TestQueries(t *testing.T) {
	qatargs := []string{"bot", "linux-list"}
	q := mlstats.QueryUntaggedAddresses(qatargs)

	sql, args, err := q.Prepared(false).ToSQL()
	if err != nil {
		t.Errorf("Preparing QueryUntaggedAddress: %v", err)
	}
	t.Logf("QueryUntaggedAddress(%v)\n SQL: %s\n Args: %v", qatargs, sql, args)

	msargs := mlstats.MessageSelection{
		Tagname:     "4.18 Dev Window",
		StartDate:   "2022-12-09",
		EndDate:     "2023-11-16",
		ExcludeTags: []string{"bot"},
	}
	q = mlstats.QueryMessageSelection(msargs)

	sql, args, err = q.Prepared(false).ToSQL()
	if err != nil {
		t.Errorf("Preparing QueryMessageSelection: %v", err)
	}
	t.Logf("QueryMessageSelection(%v)\n SQL: %s\n Args: %v", msargs, sql, args)

	msargs = mlstats.MessageSelection{
		Tagname:   "4.18 Dev Window",
		StartDate: "2022-12-09",
		EndDate:   "2023-11-16",
	}
	q = mlstats.QueryMessageSelection(msargs)

	sql, args, err = q.Prepared(true).ToSQL()
	if err != nil {
		t.Errorf("Preparing QueryMessageSelection: %v", err)
	}
	t.Logf("QueryMessageSelection(%v)\n SQL: %s\n Args: %v", msargs, sql, args)

	msargs = mlstats.MessageSelection{
		Tagname:     "4.18 Dev Window",
		StartDate:   "2022-12-09",
		EndDate:     "2023-11-16",
		ExcludeTags: []string{"bot"},
	}
	qi := mlstats.QueryInsertMessageSelection(msargs)

	sql, args, err = qi.Prepared(true).ToSQL()
	if err != nil {
		t.Errorf("Preparing QueryInsertMessageSelection: %v", err)
	}
	t.Logf("QueryInsertMessageSelection(%v)\n SQL: %s\n Args: %v", msargs, sql, args)

}
