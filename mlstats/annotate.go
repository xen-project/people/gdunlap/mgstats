package mlstats

import (
	"fmt"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"gitlab.com/xen-project/people/gdunlap/mgstats/idmap"
	"gitlab.com/xen-project/people/gdunlap/mgstats/utils"
)

type AnnoteTaggedArgs struct {
	Tagname string
}

// QueryAnnotateMessages will return a query with annotated messages, suitable
// to be inserted into the annotations table.  If filter is provided which
// includes a set of messageids, it will be joined with the message list, acting
// as a filter. If no filter is provided, the query will match all messages.
func QueryAnnotateMessages(filter *goqu.SelectDataset) *goqu.SelectDataset {
	// A table with all mails cross-matched by email in the From field
	// (envelopepart=1) with people, people by company date, and companies
	qDateMatcher := dialect.From(TLmdbMessages)

	if filter != nil {
		qDateMatcher = qDateMatcher.NaturalJoin(filter)
	}
	qDateMatcher = qDateMatcher.
		NaturalJoin(dialect.From(TLmdbEnvelopejoin).Where(goqu.C("envelopepart").Eq(1))).
		NaturalJoin(TLmdbAddresses).
		NaturalLeftJoin(idmap.QCompanyEmails).
		NaturalLeftJoin(idmap.QPersonEmailsCompanies).
		Select(goqu.L("*"),
			goqu.L(`case when date between IFNULL(startdate, '0000-01-01') and IFNULL(enddate, '9999-12-31') then personcompanyinner end`).As(idmap.CPersonCompany))

	// Above, coalesced so that each message has a single label
	return dialect.From(qDateMatcher).
		Select(
			CMessageId,
			goqu.COALESCE(idmap.CPersonName, goqu.L("mailboxname || '@' || hostname")).As(idmap.CPersonLabel),
			goqu.COALESCE(idmap.CHostCompany, goqu.MAX(idmap.CPersonCompany), goqu.L("'Unknown'")).As(idmap.CCompanyLabel),
			idmap.CHostCompany,
			idmap.CPersonCompany,
			idmap.CPersonId,
		).GroupBy(CMessageId)

}

func QueryInsertAnnotateMessages(filter *goqu.SelectDataset) *goqu.InsertDataset {
	return dialect.Insert(TAnnotations).FromQuery(
		QueryAnnotateMessages(filter)).
		OnConflict(goqu.DoNothing())
}

// AnnotateTaggedMessages will generate new annotations for messages with the
// given tag which don't yet have them; but existing annotations will not be
// touched.  If tagname is empty, all tagged messages will be annotated (again
// with the caveat that existing tags will remain). If ID information is
// updated, all the annotations should be deleted and regenerated.
func AnnotateTaggedMessagesTx(eq sqlx.Ext, args AnnoteTaggedArgs) error {
	filter := QueryTagMessageidsAll()
	if args.Tagname != "" {
		filter = QueryTagMessageids(args.Tagname)
	}

	if _, err := utils.QueryInsert(eq,
		QueryInsertAnnotateMessages(filter)); err != nil {
		return fmt.Errorf("Annotating tagged messages: %w", err)
	}

	return nil
}

// AnnotateTaggedMessages is a transaction wrapper arount AnnoteTaggedMessagesTx.
var AnnotateTaggedMessages = utils.TxWrapArg(AnnotateTaggedMessagesTx)
