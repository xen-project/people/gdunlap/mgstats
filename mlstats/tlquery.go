package mlstats

import (
	"fmt"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jmoiron/sqlx"
	"gitlab.com/xen-project/people/gdunlap/mgstats/idmap"
	"gitlab.com/xen-project/people/gdunlap/mgstats/utils"
)

type EntityStat struct {
	PersonLabel  string
	CompanyLabel string
	Count        int64
}

type Entity int

const (
	EntityPerson        = Entity(iota)
	EntityCompany       = Entity(iota)
	EntityPersonCompany = Entity(iota)
)

type EntityStatArgs struct {
	Tagname       string
	Entity        Entity
	CutOffPercent int
}

func GetEntityStatsTx(eq sqlx.Ext, args EntityStatArgs) ([]EntityStat, error) {
	if args.CutOffPercent != 0 {
		return nil, fmt.Errorf("CutOffPercent not supported yet")
	}

	var stats []EntityStat

	selects := []any{}
	groupbys := []any{}
	orderbys := []exp.OrderedExpression{}

	// Always start with a company, if that's included
	if args.Entity == EntityCompany || args.Entity == EntityPersonCompany {
		selects = append(selects, idmap.CCompanyLabel)
		groupbys = append(groupbys, idmap.CCompanyLabel)
	}

	// Then add the person, if that's included
	if args.Entity == EntityPerson || args.Entity == EntityPersonCompany {
		selects = append(selects, idmap.CPersonLabel)
		groupbys = append(groupbys, idmap.CPersonLabel)
	}

	// Only need to order by company first if we're reporting both
	if args.Entity == EntityPersonCompany {
		orderbys = append(orderbys, idmap.CCompanyLabel.Asc())
	}

	if len(selects) == 0 {
		return nil, fmt.Errorf("INTERNAL ERROR: No columns selected!")
	}

	if len(groupbys) == 0 {
		return nil, fmt.Errorf("INTERNAL ERROR: No columns in groupby!")
	}

	// Always include the count, and order by individual count descending
	selects = append(selects, goqu.COUNT(goqu.L("*")).As(goqu.I("count")))
	orderbys = append(orderbys, goqu.I("count").Desc())

	q := dialect.From(TAnnotations)

	if args.Tagname != "" {
		q = q.NaturalJoin(dialect.From(TMessageTags).Where(CTagname.Eq(args.Tagname)))
	}

	q = q.Select(selects...).
		GroupBy(groupbys...).
		Order(orderbys...)

	err := utils.QuerySelect(eq, &stats, q)

	if err != nil {
		return nil, err
	} else {
		return stats, nil
	}
}

var GetEntryStats = utils.TxWrapArgRes(GetEntityStatsTx)
