package mlstats_test

import (
	"testing"

	"gitlab.com/xen-project/people/gdunlap/mgstats/mlstats"
)

func TestQueriesAnnotate(t *testing.T) {
	q := mlstats.QueryAnnotateMessages(mlstats.QueryTagMessageids("4.18 dev window"))

	sql, args, err := q.Prepared(true).ToSQL()

	if err != nil {
		t.Errorf("ERROR Converting to SQL: %v", err)
	}

	t.Logf("sql: %v\nargs: %v", sql, args)
}

func TestAnnotate(t *testing.T) {
	db := openDatabases(t)
	if db == nil {
		return
	}

	t418 := "4.18 Dev Window"

	// Tag something for the 4.18 window, double-check the results
	t.Logf("Tagging non-bot messages from 4.18 development window")
	if err := mlstats.TagMessages(db, mlstats.MessageSelection{
		Tagname:     t418,
		StartDate:   "2022-12-09",
		EndDate:     "2023-11-16",
		ExcludeTags: []string{"bot"},
	}); err != nil {
		t.Errorf("Tagging messages: %v", err)
		return
	}

	t.Logf("Annotating those messages")
	if err := mlstats.AnnotateTaggedMessages(db, mlstats.AnnoteTaggedArgs{Tagname: t418}); err != nil {
		t.Errorf("Annotating messages: %v", err)
		return
	}

	t.Log("Querying company, people entities")
	if ents, err := mlstats.GetEntryStats(db, mlstats.EntityStatArgs{
		Tagname: t418, Entity: mlstats.EntityPersonCompany,
	}); err != nil {
		t.Errorf("Querying entities: %v", err)
	} else {
		t.Logf("Results: %v", ents)
	}
}
