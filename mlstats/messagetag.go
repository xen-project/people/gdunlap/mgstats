package mlstats

import (
	"fmt"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"gitlab.com/xen-project/people/gdunlap/mgstats/idmap"
	"gitlab.com/xen-project/people/gdunlap/mgstats/utils"
)

// FIXME: Add mailboxname
type MessageSelection struct {
	Tagname     string
	StartDate   string
	EndDate     string
	ExcludeTags []string
}

// Returns a query containing all tagged messageids
func QueryTagMessageidsAll() *goqu.SelectDataset {
	return dialect.From(TMessageTags).Select(CMessageId)
}

// Returns a query containing all messageids with the given tag.
func QueryTagMessageids(tagname string) *goqu.SelectDataset {
	return QueryTagMessageidsAll().Where(CTagname.Eq(tagname))
}

// QueryUntaggedAddress returns a SelectDataset with all lmdb_addresses *not*
// matching any of the given tags
func QueryUntaggedAddresses(tags []string) *goqu.SelectDataset {
	return dialect.From(TLmdbAddresses).
		NaturalLeftJoin(idmap.QueryTagAddress(tags)).
		Where(idmap.CTagId.IsNull())
}

// QueryMessageSelection returns a SelectDataset with lmdb_messages rows
// matching the message selection arguments.  No Select() will have been added,
// so the caller can generate their own select.
func QueryMessageSelection(args MessageSelection) *goqu.SelectDataset {
	// First, generate a query for the messages we want
	q := dialect.From(TLmdbMessages)

	if len(args.ExcludeTags) > 0 {
		q = q.NaturalJoin(TLmdbEnvelopejoin).
			NaturalJoin(QueryUntaggedAddresses(args.ExcludeTags)).
			Where(CEnvelopePart.Eq(1))
	}

	var ws []goqu.Expression

	if args.StartDate != "" {
		ws = append(ws, CDate.Gt(goqu.L("date(?)", args.StartDate)))
	}

	if args.EndDate != "" {
		ws = append(ws, CDate.Lt(goqu.L("date(?)", args.EndDate)))
	}

	if len(ws) > 0 {
		q = q.Where(ws...)
	}

	return q
}

func QueryInsertMessageSelection(args MessageSelection) *goqu.InsertDataset {
	return dialect.Insert(TMessageTags).FromQuery(
		QueryMessageSelection(args).
			Select(goqu.V(args.Tagname).As("tagname"), CMessageId))
}

func TagMessagesTx(eq sqlx.Ext, args MessageSelection) error {
	// Drop existing tags of this type
	_, err := utils.QueryDelete(eq, dialect.Delete(TMessageTags).Where(CTagname.Eq(args.Tagname)))
	if err != nil {
		return fmt.Errorf("Dropping old tags: %w", err)
	}

	_, err = utils.QueryInsert(eq, QueryInsertMessageSelection(args))

	return err
}

var TagMessages = utils.TxWrapArg(TagMessagesTx)
