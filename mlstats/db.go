package mlstats

import (
	_ "embed"
	"fmt"

	"github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/sqlite3"
	"github.com/jmoiron/sqlx"
	"gitlab.com/martyros/sqlutil/lifecycle"
	"gitlab.com/martyros/sqlutil/txutil"

	"gitlab.com/xen-project/people/gdunlap/mgstats/idmap"
)

type DbConfig struct {
	AnalysisFile  string // If this is empty, ":memory:" will be used
	IdMapFile     string
	UpgradeSchema bool
}

//go:embed mlstats.sql
var sqlMlstatsCreate string

const schemaCurrent = 1

var (
	dialect = goqu.Dialect("sqlite3")

	SMlStats = goqu.S("mlstats")

	TMessageTags = SMlStats.Table("messagetags")
	CTagname     = goqu.C("tagname")

	TAnnotations = SMlStats.Table("annotations")
	CMessageId   = goqu.C("messageid")

	/* FIXME Move these to localmaildb */
	TLmdbMessages     = goqu.T("lmdb_messages")
	CDate             = goqu.C("date")
	TLmdbAddresses    = goqu.T("lmdb_addresses")
	TLmdbEnvelopejoin = goqu.T("lmdb_envelopejoin")
	CEnvelopePart     = goqu.C("envelopepart")
)

func attach(eq sqlx.Ext, dbc DbConfig) error {
	var fname string

	if dbc.AnalysisFile == "" {
		fname = ":memory:"
	} else {
		fname = fmt.Sprintf("file:%s", dbc.AnalysisFile)
	}
	if _, err := eq.Exec(fmt.Sprintf(`attach "%s" as mlstats`, fname)); err != nil {
		return err
	}

	lcfg := lifecycle.Schema(schemaCurrent,
		lifecycle.Recipe{Sql: sqlMlstatsCreate}).
		SetSchemaName("gitlab.com/xen-project/people/gdunlap/mgstat/mlstats").
		SetParamTableName("mlstats.attributes").
		SetAutoMigrate(dbc.UpgradeSchema)

	if err := lcfg.OpenTx(eq); err != nil {
		return fmt.Errorf("Opening idmap database: %w", err)
	}

	return nil
}

// Open returns an SQLx db with both the analysis DB and the idmap attached.
// Because they are primarily meant to be attached as a secondary database, it
// opens an empty "memory" database and attaches them to it.
func Open(dbc DbConfig) (*sqlx.DB, error) {
	DB, err := sqlx.Open("sqlite3", ":memory:?cache=shared&_foreign_keys=on")
	if err != nil {
		return nil, err
	}

	err = txutil.TxLoopDb(DB, func(eq sqlx.Ext) error {
		return AttachTx(eq, dbc)
	})

	if err != nil {
		return nil, err
	}

	return DB, nil
}

// Attach attaches the idmap database as `idmap` to an existing sqlite3 database
func AttachTx(eq sqlx.Ext, dbc DbConfig) error {
	if err := attach(eq, dbc); err != nil {
		return fmt.Errorf("Attaching analysis db: %w", err)
	}

	return idmap.Attach(eq, idmap.DbConfig{Filename: dbc.IdMapFile, UpgradeSchema: dbc.UpgradeSchema})
}
