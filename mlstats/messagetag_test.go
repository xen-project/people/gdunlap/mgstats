package mlstats_test

import (
	"os"
	"testing"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"gitlab.com/martyros/sqlutil/txutil"
	"gitlab.com/xen-project/people/gdunlap/mgstats/mlstats"
	"gitlab.com/xen-project/people/gdunlap/mgstats/utils"
)

var dialect = goqu.Dialect("sqlite3")

func openDatabases(t *testing.T) *sqlx.DB {
	cfg := mlstats.DbConfig{
		IdMapFile: "idmap.sqlite",
	}

	lmdbFile := "lmdb.sqlite"

	// Check to see if idmap.sqlite and lmdb.sqlite exist
	if _, err := os.Stat(lmdbFile); err != nil {
		t.Skipf("Couldn't open default lmdb file %s, skipping", lmdbFile)
	}

	if _, err := os.Stat(cfg.IdMapFile); err != nil {
		t.Skipf("Couldn't open default idmap sile %s, skipping", cfg.IdMapFile)
	}

	// Create temporary mlstat file
	if mlstatFile, err := os.CreateTemp("", "mlstat*.sqlite"); err != nil {
		t.Errorf("ERROR: Opening temp file for mlstat: %v", err)
		return nil
	} else {
		cfg.AnalysisFile = mlstatFile.Name()
	}

	// Open lmdb file
	db, err := sqlx.Open("sqlite3", "file:"+lmdbFile+"?_fk=true&mode=rwc")
	if err != nil {
		t.Errorf("Opening lmdb database: %v", err)
	}

	// Attach idmap, mlstats
	err = txutil.TxLoopDb(db, func(eq sqlx.Ext) error {
		return mlstats.AttachTx(eq, cfg)
	})
	if err != nil {
		t.Errorf("mlstats.Attachtx(%v): %v", cfg, err)
	}

	t.Logf("Attached %v", cfg)

	return db
}

func TestMessageTag(t *testing.T) {
	db := openDatabases(t)

	if db == nil {
		return
	}

	t.Logf("Getting message count, start, and end")

	{
		// Count all the messages, getting the earliest and latest dates
		stat1 := struct {
			Count int64
			Start string
			End   string
		}{}

		// Chose dates A and B, about 1/3 and 2/3 of the way through
		if err := utils.QueryGet(db, &stat1,
			dialect.From("lmdb_messages").
				Select(goqu.COUNT(goqu.L("*")).As("count"),
					goqu.MIN(goqu.L("date")).As("start"),
					goqu.MAX(goqu.L("date")).As("end"))); err != nil {
			t.Errorf("ERROR Querying total messages: %v", err)
			return
		}

		t.Logf("Stat1: %v", stat1)

		start, err := time.Parse("2006-01-02 15:04:05-07:00", stat1.Start)
		if err != nil {
			t.Errorf("Parsing start time %s: %v", stat1.Start, err)
			return
		}

		end, err := time.Parse("2006-01-02 15:04:05-07:00", stat1.End)
		if err != nil {
			t.Errorf("Parsing end time %s: %v", stat1.End, err)
			return
		}

		atime := time.Unix((end.Unix()-start.Unix())/3+start.Unix(), 0)

		btime := time.Unix((end.Unix()-start.Unix())*2/3+start.Unix(), 0)

		t.Logf("start: %v a: %v b: %v end: %v", start, atime, btime, end)

		// Get create for ..A, A..B, and B..
		t.Logf("Creating tag ..A")
		if err := mlstats.TagMessages(db, mlstats.MessageSelection{
			Tagname: "NilToA",
			EndDate: atime.Format("2006-01-02"),
		}); err != nil {
			t.Errorf("Tagging ..A: %v", err)
			return
		}

		t.Logf("Creating tag A..B")
		if err := mlstats.TagMessages(db, mlstats.MessageSelection{
			Tagname:   "AToB",
			StartDate: atime.Format("2006-01-02"),
			EndDate:   btime.Format("2006-01-02"),
		}); err != nil {
			t.Errorf("Tagging ..A: %v", err)
			return
		}

		t.Logf("Creating tag B..")
		if err := mlstats.TagMessages(db, mlstats.MessageSelection{
			Tagname:   "BToNil",
			StartDate: btime.Format("2006-01-02"),
		}); err != nil {
			t.Errorf("Tagging ..A: %v", err)
			return
		}

		// Count messages in each, summing them up and ensuring that they add up to the total
		var cNilToA, cAToB, cBToNil int64

		if err := utils.QueryGet(db, &cNilToA,
			dialect.From(mlstats.TMessageTags).
				Select(goqu.COUNT(goqu.L("*"))).
				Where(mlstats.CTagname.Eq("NilToA"))); err != nil {
			t.Errorf("ERROR Querying tagged messages: %v", err)
			return
		}

		if err := utils.QueryGet(db, &cAToB,
			dialect.From(mlstats.TMessageTags).
				Select(goqu.COUNT(goqu.L("*"))).
				Where(mlstats.CTagname.Eq("AToB"))); err != nil {
			t.Errorf("ERROR Querying tagged messages: %v", err)
			return
		}

		if err := utils.QueryGet(db, &cBToNil,
			dialect.From(mlstats.TMessageTags).
				Select(goqu.COUNT(goqu.L("*"))).
				Where(mlstats.CTagname.Eq("BToNil"))); err != nil {
			t.Errorf("ERROR Querying tagged messages: %v", err)
			return
		}

		t.Logf("Message numbers: %d %d %d", cNilToA, cAToB, cBToNil)

		if cNilToA+cAToB+cBToNil != stat1.Count {
			t.Errorf("ERROR: Expected sum to be %d, got %d!", stat1.Count, cNilToA+cAToB+cBToNil)
		}
	}

	{
		// Tag something for the 4.18 window, double-check the results
		t.Logf("Tagging non-bot messages from 4.18 development window")
		if err := mlstats.TagMessages(db, mlstats.MessageSelection{
			Tagname:     "4.18 Dev Window",
			StartDate:   "2022-12-09",
			EndDate:     "2023-11-16",
			ExcludeTags: []string{"bot"},
		}); err != nil {
			t.Errorf("Tagging messages: %v", err)
			return
		}

		var mcount int64
		if err := utils.QueryGet(db, &mcount,
			dialect.From(mlstats.TMessageTags).
				Select(goqu.COUNT(goqu.L("*"))).
				Where(mlstats.CTagname.Eq("4.18 Dev Window"))); err != nil {
			t.Errorf("Getting count of 4.18 dev window tags: %v", err)
		}

		t.Logf("Non-bot messages in 4.18 dev window: %d", mcount)
	}

}
