create table mlstats.messagetags(
    tagname   text not null,
    messageid text not null  /* References lmdb_messages */  
);

create table mlstats.annotations(
    messageid     text primary key, /* References lmdb_messages */
    personlabel   text not null,    /* personname of personid if present, or email address if unknown */
    companylabel  text not null,    /* companyname if present, or "Unknown" if unknown */
    hostcompany   text,             /* Company as indicated by hostname_to_company (if any) */
    personcompany text,             /* Company as indicated by person_to_company (if any) */
    personid      int               /* Person ID, if any */
)
