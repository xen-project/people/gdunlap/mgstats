package utils

import (
	"database/sql"
	"fmt"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"gitlab.com/martyros/sqlutil/txutil"
)

func QuerySelect(q sqlx.Queryer, t any, query *goqu.SelectDataset) error {
	if sql, args, err := query.Prepared(true).ToSQL(); err != nil {
		return err
	} else if err = sqlx.Select(q, t, sql, args...); err != nil {
		return err
	}
	return nil
}

func QueryGet(q sqlx.Queryer, t any, query *goqu.SelectDataset) error {
	if sql, args, err := query.Prepared(true).ToSQL(); err != nil {
		return err
	} else if err = sqlx.Get(q, t, sql, args...); err != nil {
		return err
	}
	return nil
}

// Obviously this is a slight inadequacy of the golang interface / generics
// system: The following code is identical, but we have to write two copies of
// it becaufe we want to call both Prepared(true) *and* ToSQL
func QueryInsert(e sqlx.Execer, query *goqu.InsertDataset) (sql.Result, error) {
	if sql, args, err := query.Prepared(true).ToSQL(); err != nil {
		return nil, err
	} else {
		res, err := e.Exec(sql, args...)

		if err != nil {
			err = fmt.Errorf("Prepared Insert failed. err %w  SQL: %v args: %v", err, sql, args)
		}

		return res, err
	}
}

func QueryDelete(e sqlx.Execer, query *goqu.DeleteDataset) (sql.Result, error) {
	if sql, args, err := query.Prepared(true).ToSQL(); err != nil {
		return nil, err
	} else {
		res, err := e.Exec(sql, args...)

		if err != nil {
			err = fmt.Errorf("Prepared Delete failed. err %w  SQL: %v args: %v", err, sql, args)
		}

		return res, err
	}
}

// TxWrapArg takes a function which accepts an sqlx.Ext and an argument and
// returns an error, and returns a function which accepts an *sqlx.DB and an
// argument and returns an error.  The returned function will call the second
// function in a txloop.
func TxWrapArg[A any](f func(sqlx.Ext, A) error) func(db *sqlx.DB, args A) error {
	return func(db *sqlx.DB, args A) error {
		return txutil.TxLoopDb(db, func(eq sqlx.Ext) error { return f(eq, args) })
	}
}

// TxWrapArgRes takes a function which accepts an sqlx.Ext and an argement, and
// returns a result and an error; and it returns a function with an identical
// function signature except with an *sqlx.DB instead.  The returned function
// call will call the second in a txloop, returning the result of the inner
// function.
func TxWrapArgRes[A any, R any](f func(sqlx.Ext, A) (R, error)) func(db *sqlx.DB, args A) (R, error) {
	return func(db *sqlx.DB, args A) (R, error) {
		var r R

		err := txutil.TxLoopDb(db, func(eq sqlx.Ext) error {
			var err error

			r, err = f(eq, args)

			return err
		})

		return r, err

	}
}
