package gitdb

import (
	//"database/sql"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"

	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/martyros/sqlutil/txutil"
)

var schemaVersionCurrent = schemaVersion(2)

type GitDB struct {
	*sqlx.DB

	// FIXME: Hacky
	limitTime time.Time
}

func (db *GitDB) txLoop(txFunc func(eq sqlx.Ext) error) error {
	return txutil.TxLoopDb(db.DB, txFunc)
}

type Reftype string

var (
	ReftypeTag    = Reftype("tag")
	ReftypeBranch = Reftype("branch")
)

type openOptionStruct struct {
	origin    string
	update    bool
	limitTime time.Time
}

type OpenOption func(opts *openOptionStruct) error

// Specify the git url origin.  Required when opening a new database.
// If opening an existing database, Open will fail if the origin
// doesn't match.
func OpenOrigin(origin string) OpenOption {
	return func(opt *openOptionStruct) error {
		opt.origin = origin
		return nil
	}
}

// Pull in new data from the origin even if the database exists
func OpenUpdate() OpenOption {
	return func(opt *openOptionStruct) error {
		opt.update = true
		return nil
	}
}

// Stop processing commits older than this age
func OpenCommitLimitDuration(d time.Duration) OpenOption {
	return func(opt *openOptionStruct) error {
		opt.limitTime = time.Now().Add(-d)
		return nil
	}
}

func Open(filename string, opts ...OpenOption) (*GitDB, error) {
	var opt openOptionStruct
	for _, f := range opts {
		if err := f(&opt); err != nil {
			return nil, err
		}
	}

	var (
		err               error
		fileSchemaVersion schemaVersion
	)

	db := GitDB{limitTime: opt.limitTime}

	logrus.Infof("Opening database %s", filename)
	db.DB, err = sqlx.Open("sqlite3",
		fmt.Sprintf("file:%s?cache=shared&_foreign_keys=true&_journal=WAL", filename))
	if err != nil {
		return nil, err
	}

	logrus.Debug("Checking db schema version")

	tx, err := db.Beginx()
	if err != nil {
		goto out_close
	}

	fileSchemaVersion, err = getSchemaVersion(tx)
	if err != nil {
		goto out_rollback
	}

	// No 'attributes' table?  Assume an empty database
	if fileSchemaVersion == 0 {
		if opt.origin == "" {
			err = fmt.Errorf("Origin must be specified when creating database")
			goto out_rollback
		}

		opt.update = true

		err = initSchema(tx, opt.origin)
		if err != nil {
			goto out_rollback
		}
		goto commit
	}

	if fileSchemaVersion != schemaVersionCurrent {
		err = fmt.Errorf("Invalid DB schema %d", fileSchemaVersion)
		goto out_rollback
	}

commit:
	tx.Commit()

	if opt.update {
		err = db.Update()
		if err != nil {
			goto out_close
		}
	}

	return &db, nil

out_rollback:
	tx.Rollback()
out_close:
	db.Close()

	db.DB = nil

	return nil, err

}

func initSchema(tx *sqlx.Tx, origin string) error {
	logrus.Info("Creating database tables")

	_, err := tx.Exec(`
        CREATE TABLE attributes(
           key text primary key,
           value text not null);
`)
	if err != nil {
		return fmt.Errorf("Creating table attributes: %v", err)
	}

	err = setSchemaVersion(tx, schemaVersionCurrent)

	if err != nil {
		return fmt.Errorf("Setting schema value: %v", err)
	}

	_, err = tx.Exec(`
CREATE TABLE commits(
	commithash   text primary key,
	authorsid    integer not null,
	authorts     string not null,
	committersid integer not null,
	committerts  string not null,
	message      string not null,
	pgpsignature text not null, /* "" is empty */
		
	foreign key(authorsid) references signatures(sid),
	foreign key(committersid) references signatures(sid)
)`)
	if err != nil {
		return fmt.Errorf("Creating table commits: %v", err)
	}

	_, err = tx.Exec(`
CREATE TABLE signatures(
       sid          integer primary key,
       personalname text not null,
	   mailboxname  text not null,
       hostname     text not null
)`)
	if err != nil {
		return fmt.Errorf("Creating table signatures: %v", err)
	}

	_, err = tx.Exec(`
CREATE TABLE commit_signature_tags(
		tagname string,
		sid     integer references signatures(sid),
		idx     integer,
		commithash text references commits(commithash),
		unique(tagname, sid, idx, commithash)
	  )`)
	if err != nil {
		return fmt.Errorf("Creating table commit_signature_tags: %v", err)
	}

	_, err = tx.Exec(`
CREATE TABLE commitjoin(
        parenthash text not null,
	childhash  text not null,
	primary key(parenthash, childhash),
	/*foreign key(parenthash) references commits(commithash),*/
	foreign key(childhash)  references commits(commithash)
)`)
	if err != nil {
		return fmt.Errorf("Creating table commitjoin: %v", err)
	}

	_, err = tx.Exec(`
CREATE TABLE commitfilestat(
       commithash text,
       filename   text,
       addition integer not null,
       deletion integer not null,
       foreign key(commithash) references commits(commithash),
       primary key (commithash, filename)
)`)
	if err != nil {
		return fmt.Errorf("Creating table commitfilestat: %v", err)
	}

	_, err = tx.Exec(`
CREATE TABLE refs(
       reftype text, /* 'head' or 'tag' */
       refname text,
       commithash text not null,
       primary key (refname, reftype)
       foreign key(commithash) references commits(commithash)
)`)
	if err != nil {
		return fmt.Errorf("Creating table refs: %v", err)
	}

	_, err = tx.Exec(`
CREATE TABLE ref_commit_list(
       reftype text not null,
       refname text not null,
       commithash text not null,
       foreign key(reftype,refname) references refs(reftype,refname),
       foreign key(commithash) references commits(commithash),
	   unique(reftype, refname, commithash)
);`)
	if err != nil {
		return fmt.Errorf("Creating table ref_commit_list: %v", err)
	}

	_, err = tx.Exec(`
insert into attributes(key, value) values (?, ?)
`, attributeGitOrigin, origin)
	if err != nil {
		return fmt.Errorf("Setting origin to %s", origin)
	}

	return nil
}
