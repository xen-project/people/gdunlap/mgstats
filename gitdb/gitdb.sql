/* 
 * Table to contain database attributes.
 * Current attributes:
 * "schema_version", "1"
 */
CREATE TABLE attributes(
    key   text primary key,
    value text not null);
    /* "schema_version" "1" */


/*
 * Kinds of questions we want to answer:
 * - What is the rate of commits over time?
 * - What is the rate of code changes over time?
 * - What does the diversity of contribution look like over time?
 * 
 */

CREATE TABLE commits(
	commithash   text primary key,
	authorsid    integer not null,
	authorts     string not null,
	committersid integer not null,
	committerts  string not null,
	message      string not null,
	pgpsignature text not null, /* "" is empty */
		
	foreign key(authorsid) references signatures(sid),
	foreign key(committersid) references signatures(sid)
);

CREATE TABLE signatures(
       sid          integer primary key,
       personalname text not null,
       mailboxname  text not null,
       hostname     text not null,
       unique(personalname, mailboxname, hostname)
);

CREATE TABLE commit_signature_tags(
  tagname string,
  sid     integer references signatures(sid),
  idx     integer,
  commithash text references commits(commithash),
  unique(tagname, sid, idx, commithash)
);

/* NB we don't enforce a foreign key on parenthash, so that we can do partial imports */
CREATE TABLE commitjoin(
        parenthash text not null,
	childhash  text not null,
	primary key(parenthash, childhash),
	/*foreign key(parenthash) references commits(commithash),*/
	foreign key(childhash)  references commits(commithash),
);

CREATE TABLE commitfilestat(
       commithash text,
       filename   text,
       addition integer not null,
       deletion integer not null,
       foreign key(commithash) references commits(commithash),
       primary key(commithash, filename)
);

CREATE TABLE refs(
       reftype text, /* 'head' or 'tag' */
       refname text,
       commithash text not null,
       primary key (refname, reftype)
       foreign key(commithash) references commits(commithash)
);

/* Easy way to get all the commits on a branch */
CREATE TABLE ref_commit_list(
       reftype text not null,
       refname text not null,
       commithash text not null,
       foreign key(reftype,refname) references refs(reftype,refname),
       foreign key(commithash) references commits(commithash),
       unique(reftype, refname, commithash)
);

/* This turns out to be too slow when there are many commits and refs */
/*
CREATE VIEW ref_commit_list AS
    WITH RECURSIVE ref_commit_list(reftype, refname, commithash)
      AS (SELECT reftype, refname, commithash FROM refs
          UNION SELECT reftype, refname, parenthash
                  FROM commitjoin, ref_commit_list
	          WHERE childhash=ref_commit_list.commithash)
    SELECT * from ref_commit_list;
*/
  

/* Clause to generate commit list on the fly, making
 * sure to include only valid commits */
with recursive clist(reftype, refname, commithash)
  as (select reftype, refname, commithash from refs
      union select reftype, refname, parenthash
              from commitjoin, clist
	      where childhash=clist.commithash)
insert into ref_commit_list
  select * from clist
    where commithash not in
      (select parenthash from commitjoin
	      where parenthash not in (select commithash from commits));

/* Query commits on staging since the 4.14 branch */
select commithash, committerts
  from ref_commit_list natural join commits
  where refname="staging"
        and commithash not in
          (select commithash from ref_commit_list
	   where refname="RELEASE-4.14.0");

/* Cumulative commits to staging since 4.14 branch */
select commithash, committerts, row_number() over rowwindow
   from (select commithash, committerts
  from ref_commit_list natural join commits
  where refname="staging"
        and commithash not in
          (select commithash from ref_commit_list
	   where refname="RELEASE-4.17.0"))
  window rowwindow as (order by committerts)
  limit 20;

/* Sum of file "Addition" stats for the commit */
select commithash, committerts, sum(addition)
  from commits natural join commitfilestat
  group by commithash;

/* Cumulative "addition" stats for commits to staging since 4.14 branch */
select commithash, committerts, additions, sum(additions) over rowwindow
   from (select commithash, committerts, sum(addition) as additions
         from (select commithash, committerts
                from ref_commit_list natural join commits
                where refname="staging"
                and commithash not in
                  (select commithash from ref_commit_list
	           where refname="RELEASE-4.14.0"))
	     natural join commitfilestat
	     group by commithash)
  window rowwindow as (order by committerts)
  limit 20;

/* File, total commits changing file for the 4.14 release, into a generic tree structure */
select filename as name, count(*) as value
    from commitfilestat
    where commithash in
        (select commithash
                from ref_commit_list natural join commits
                where refname="staging"
                and commithash not in
                  (select commithash from ref_commit_list
	           where refname="RELEASE-4.14.0"))
    group by name
    order by value desc
    limit 20;

/* Find commit hash, author sid, tag sid where authorsid != first SoB sid */
select commits.commithash, commits.authorsid, a.sid
  from commits join
       (select * from commit_signature_tags
          where tagname="Signed-off-by" and idx=1) as a
	on commits.commithash=a.commithash
  where a.sid != commits.authorsid;

/*
 * Number of commits by email of first SoB
 */
with targetcommits(commithash) as (
  select ref_commit_list.commithash
    from ref_commit_list
    where refname="RELEASE-4.18.0"
      and ref_commit_list.commithash not in
        (select commithash from ref_commit_list where refname="staging-4.17"))
select mailboxname || "@" || hostname as email, count(*) as commitcount
  from targetcommits
    join commit_signature_tags on targetcommits.commithash=commit_signature_tags.commithash
    join signatures on commit_signature_tags.sid=signatures.sid
    where tagname="Signed-off-by" and idx=1
  group by email
  order by commitcount desc;

/*
 * Number of commits by person and company
 */
with targetcommits(commithash) as (
  select ref_commit_list.commithash
    from ref_commit_list
    where refname="RELEASE-4.18.0"
      and ref_commit_list.commithash not in
        (select commithash from ref_commit_list where refname="staging-4.17")),
annotated_commits as (
select *, coalesce(hostcompany, max(personcompany), 'Unknown') as companylabel,
       IFNULL(personname, mailboxname || "@" || hostname) as personlabel
   from (
      select *,
        case when ts between IFNULL(startdate, '0000-01-01') and IFNULL(enddate, '9999-12-31') then personcompanyinner end as personcompany
      from targetcommits
        join (
	  select commithash,
	    strftime('%Y-%m-%d %H:%M:%S', replace(replace(authorts, 'T', ' '), 'Z', '')) as ts
	  from commits
       ) cts on targetcommits.commithash=cts.commithash
        join commit_signature_tags on targetcommits.commithash=commit_signature_tags.commithash
    	join signatures on commit_signature_tags.sid=signatures.sid
        left join (
          select hostname, companyname as hostcompany
            from hostname_to_company natural join companies) h on h.hostname=signatures.hostname
        left join (
          select personname, mailboxname, hostname, startdate, enddate, companyname as personcompanyinner
            from address_to_person
              natural join person
              left natural join person_to_company
              left natural join companies) p on p.hostname=signatures.hostname and p.mailboxname=signatures.mailboxname
  where tagname="Signed-off-by" and idx=1)
  group by commithash )
select companylabel, count(*) as commitcount
  from annotated_commits
  group by companylabel
  order by commitcount desc;

/*
 * Number of commits by person, limited to 20
 */
with targetcommits(commithash) as (
  select ref_commit_list.commithash
    from ref_commit_list
    where refname="RELEASE-4.18.0"
      and ref_commit_list.commithash not in
        (select commithash from ref_commit_list where refname="staging-4.17")),
annotated_commits as (
select *, coalesce(hostcompany, max(personcompany), 'Unknown') as companylabel,
       IFNULL(personname, mailboxname || "@" || hostname) as personlabel
   from (
      select *,
        case when ts between IFNULL(startdate, '0000-01-01') and IFNULL(enddate, '9999-12-31') then personcompanyinner end as personcompany
      from targetcommits
        join (
          select commithash,
            strftime('%Y-%m-%d %H:%M:%S', replace(replace(authorts, 'T', ' '), 'Z', '')) as ts
          from commits
       ) cts on targetcommits.commithash=cts.commithash
        join commit_signature_tags on targetcommits.commithash=commit_signature_tags.commithash
        join signatures on commit_signature_tags.sid=signatures.sid
        left join (
          select hostname, companyname as hostcompany
            from hostname_to_company natural join companies) h on h.hostname=signatures.hostname
        left join (
          select personname, mailboxname, hostname, startdate, enddate, companyname as personcompanyinner
            from address_to_person
              natural join person
              left natural join person_to_company
              left natural join companies) p on p.hostname=signatures.hostname and p.mailboxname=signatures.mailboxname
  where tagname="Signed-off-by" and idx=1)
  group by commithash ),
ranked_commits as (
select personlabel, count(*) as commitcount,
       rank() over (order by count(*) desc) as rank
  from annotated_commits
  group by personlabel)
select case 
         when rank <= 19 then personlabel 
         else 'Other' 
       end as personlabel,
       sum(commitcount) as commitcount
from ranked_commits
group by case 
           when rank <= 19 then personlabel 
           else 'Other' 
         end
order by sum(commitcount) desc;


annotated_commits as
  (select *, coalesce(hostcompany, max(personcompany), 'Unknown') as companyname
    from (
      select *,
        case when date between IFNULL(startdate, '0000-01-01') and IFNULL(enddate, '9999-12-31') then personcompanyinner end as personcompany
      from targetcommits
        join commit_signature_tags on targetcommits.commithash=commit_signature_tags.commithash
    	join signatures on commit_signature_tags.sid=signatures.sid
        left join (
          select hostname, companyname as hostcompany
            from hostname_to_company natural join companies) h on h.hostname=signatures.hostname
        left natural join (
          select personname, mailboxname, hostname, startdate, enddate, companyname as personcompanyinner
            from address_to_person
              natural join person
              left natural join person_to_company
              left natural join companies)
       where envelopepart=1)
    group by messageid),

select mailboxname || "@" || hostname as email, count(*) as commitcount
  from targetcommits
    join commit_signature_tags on targetcommits.commithash=commit_signature_tags.commithash
    join signatures on commit_signature_tags.sid=signatures.sid
    where tagname="Signed-off-by" and idx=1
  group by email
  order by commitcount desc;

select commithash, committerts
  from ref_commit_list natural join commits
  where refname="staging"
        and commithash not in
          (select commithash from ref_commit_list
	   where refname="RELEASE-4.17.0")
  order by committerts;
