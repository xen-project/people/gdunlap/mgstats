package gitdb_test

import (
	"os"
	"testing"

	"github.com/doug-martin/goqu/v9"
	"gitlab.com/xen-project/people/gdunlap/mgstats/gitdb"
	"gitlab.com/xen-project/people/gdunlap/mgstats/idmap"
	"gitlab.com/xen-project/people/gdunlap/mgstats/utils"
)

const (
	EnvTestGitDbFile = "GOTEST_GITDB"
	EnvTestIdmapFile = "GOTEST_IDMAP"
)

func TestAnnotationQuery(t *testing.T) {
	gitdbfile := os.Getenv(EnvTestGitDbFile)
	if gitdbfile == "" {
		t.Skipf("%s not defined, skipping", EnvTestGitDbFile)
	}

	idmapfile := os.Getenv(EnvTestIdmapFile)
	if idmapfile == "" {
		t.Skipf("%s not defined, skipping", EnvTestIdmapFile)
	}

	gdb, err := gitdb.Open(gitdbfile)
	if err != nil {
		t.Errorf("Opening gitdb file %s: %v", gitdbfile, err)
	}

	err = idmap.Attach(gdb.DB, idmap.DbConfig{Filename: idmapfile})
	if err != nil {
		t.Errorf("Attaching idmapfile %s: %v", idmapfile, err)
	}

	// Commits on the staging branch
	q := gitdb.QueryCommitSelection(gitdb.CommitSelection{
		Refname:   "staging",
		StartDate: "2024-04-01",
	})

	// Add signatures and annotations
	q = gitdb.QueryAnnotateSignatures(q)

	// Now actually do a query on it
	q = goqu.From(q).
		Select(gitdb.MCommitHash, gitdb.MTagname, gitdb.MIdx, idmap.CPersonLabel, idmap.CCompanyLabel)

	if sql, args, err := q.Prepared(true).ToSQL(); err != nil {
		t.Errorf("Error preparing SQL: %v", err)
		return
	} else {
		t.Logf("SQL: %s\nargs: %v", sql, args)
	}

	res := []struct {
		CommitHash   gitdb.Hash
		Tagname      string
		Idx          int
		Personlabel  string
		Companylabel string
	}{}

	if err := utils.QuerySelect(gdb, &res, q); err != nil {
		t.Errorf("Query: %v", err)
	} else {
		t.Logf("Got %d results", len(res))
		for _, r := range res {
			t.Logf("%v", r)
		}
	}

}
