package gitdb

import (
	"github.com/doug-martin/goqu/v9"

	"gitlab.com/xen-project/people/gdunlap/mgstats/idmap"
)

var (
	dialect = goqu.Dialect("sqlite")

	TCommits         = goqu.T("commits")
	CCommitterTsDate = goqu.L("date(committerts)")

	TSignatures          = goqu.T("signatures")
	TCommitSignatureTags = goqu.T("commit_signature_tags")
	CTagname             = TCommitSignatureTags.Col("tagname")
	CIdx                 = TCommitSignatureTags.Col("idx")

	TRefCommitList = goqu.T("ref_commit_list")
	CRefname       = TRefCommitList.Col("refname")

	MCommitHash = goqu.C("commithash")
	MTagname    = goqu.C("tagname")
	MIdx        = goqu.C("idx")
)

// QueryRefCommithash returns a select dataset from ref_commit_list with the
// given refname
func QueryRefCommithash(refname string) *goqu.SelectDataset {
	return dialect.From(TRefCommitList).Select(MCommitHash).Where(CRefname.Eq(refname))
}

type CommitSelection struct {
	Refname   string
	StartDate string
	EndDate   string
}

func QueryCommitSelection(args CommitSelection) *goqu.SelectDataset {
	q := dialect.From(TCommits)

	if args.Refname != "" {
		q = q.NaturalJoin(QueryRefCommithash(args.Refname))
	}

	var ws []goqu.Expression

	if args.StartDate != "" {
		ws = append(ws, CCommitterTsDate.Gt(goqu.L("date(?)", args.StartDate)))
	}

	if args.EndDate != "" {
		ws = append(ws, CCommitterTsDate.Lte(goqu.L("date(?)", args.EndDate)))
	}

	if len(ws) > 0 {
		q = q.Where(ws...)
	}
	return q
}

// Takes a select dataset containing commit hashes and a committerts, joins them
// with the signatures, and annotates the signatures using committerts as the
// date for the company
func QueryAnnotateSignatures(q *goqu.SelectDataset) *goqu.SelectDataset {
	q = q.
		NaturalJoin(TCommitSignatureTags).
		NaturalJoin(TSignatures).
		NaturalLeftJoin(idmap.QCompanyEmails).
		NaturalLeftJoin(idmap.QPersonEmailsCompanies).
		Select(goqu.L("*"),
			goqu.L(`case when date(committerts) between IFNULL(startdate, '0000-01-01') and IFNULL(enddate, '9999-12-31') then personcompanyinner end`).As(idmap.CPersonCompany))

	return dialect.From(q).
		Select(
			goqu.L("*"),
			goqu.COALESCE(idmap.CPersonName, goqu.L("mailboxname || '@' || hostname")).As(idmap.CPersonLabel),
			goqu.COALESCE(idmap.CHostCompany, goqu.MAX(idmap.CPersonCompany), goqu.L("'Unknown'")).As(idmap.CCompanyLabel),
			idmap.CHostCompany,
			idmap.CPersonCompany,
			idmap.CPersonId,
		).GroupBy(MCommitHash, goqu.C("tagname"), goqu.C("idx"))

}
