package gitdb

import (
	//"fmt"
	"strconv"

	"github.com/jmoiron/sqlx"
)

type attributeKey string

var (
	attributeSchemaVersion = attributeKey("schema_version")
	attributeGitOrigin     = attributeKey("git_orgin")
)

type schemaVersion int

func setAttribute(tx *sqlx.Tx, attribute attributeKey, val string) error {
	_, err := tx.Exec(`insert into attributes(key, value) values(?, ?)`,
		attribute, val)
	return err
}

func getAttribute(q sqlx.Queryer, attribute attributeKey) (val string, err error) {
	err = sqlx.Get(q, &val, `select value from attributes where key=?`,
		attribute)
	return
}

func setSchemaVersion(tx *sqlx.Tx, fileSchemaVersion schemaVersion) error {
	return setAttribute(tx,
		attributeSchemaVersion,
		strconv.Itoa(int(fileSchemaVersion)))
}

//lint:ignore U1000 We may use this in the future
func updateSchemaVersion(tx *sqlx.Tx, newSchemaVersion schemaVersion) error {
	_, err := tx.Exec(`update attributes set value=? where key=?`,
		strconv.Itoa(int(newSchemaVersion)), attributeSchemaVersion)
	return err
}

func getSchemaVersion(tx *sqlx.Tx) (schemaVersion, error) {
	var tableCount int

	// Does the 'attributes' table exist?
	err := tx.Get(&tableCount,
		"SELECT count(*) FROM sqlite_master WHERE type='table' AND name='attributes';")
	if err != nil {
		return -1, err
	}

	// No tables named 'attributes'; return 0 to indicate no schema
	if tableCount == 0 {
		return 0, nil
	}

	var fileSchemaVersionString string

	// The current schema version
	fileSchemaVersionString, err = getAttribute(tx, attributeSchemaVersion)
	if err != nil {
		return -1, err
	}

	fileSchemaVersion, err := strconv.Atoi(fileSchemaVersionString)

	return schemaVersion(fileSchemaVersion), err
}
