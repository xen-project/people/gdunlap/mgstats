package gitdb

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/martyros/sqlutil/types/timedb"
	"gonum.org/v1/plot/plotter"
)

// FIXME: Move this into a separate package

func durationToDays(d time.Duration) float64 {
	return float64(d) / float64(time.Second*60*60*24)
}

// Return cumulative commits in the commit history of RefIn, but not
// in the commit history of RefNotIn.
//
// For example, db.PlotCommitsCumul("RELEASE-4.13.0",
// "RELEASE-4.14.0") will return a plot of <commit offset, ncommits>
// from the 4.14 development timeline.
//
// By contrast, db.PlotCommitsCumul("RELEASE-4.14.0",
// "RELEASE-4.13.0") will return a plot of <commit offset, ncommits>
// from the time of the 4.13 branch until the actual release.
//
// X value is time offset in (fractional) days, starting at 0. Y is the
// cumulative number of commits.
func (db *GitDB) PlotCommitsCumul(RefNotIn, RefIn string) (plotter.XYs, error) {
	vals := []struct {
		CommitterTs timedb.Time
		CCount      int
	}{}

	logrus.Infof("Querying ")
	err := db.Select(&vals, `
select committerts, row_number() over rowwindow as ccount
   from (select commithash, committerts
         from ref_commit_list natural join commits
         where refname=?
           and commithash not in
           (select commithash from ref_commit_list
	        where refname=?))
    window rowwindow as (order by committerts)
`, RefIn, RefNotIn)
	if err != nil {
		return nil, fmt.Errorf("Getting cumulative commits: %v", err)
	}

	logrus.Infof("Found %d data points", len(vals))

	xys := make(plotter.XYs, len(vals))
	if len(vals) != 0 {
		baseTime := vals[0].CommitterTs

		for i := range vals {
			xys[i].X = durationToDays(vals[i].CommitterTs.Sub(baseTime.Time))
			xys[i].Y = float64(vals[i].CCount)
		}
	}

	return xys, nil
}

// Return cumulative additions in the commit history of RefIn, but not
// in the commit history of RefNotIn.  NB that file renames are not
// included in additions.
//
// X value is time offset in (fractional) days, starting at 0. Y is the
// cumulative number of additions in the patch
func (db *GitDB) PlotAdditionsCumul(RefNotIn, RefIn string) (plotter.XYs, error) {
	vals := []struct {
		CommitterTs timedb.Time
		CAdditions  int
	}{}

	logrus.Infof("Querying ")
	err := db.Select(&vals, `
select committerts, sum(additions) over rowwindow as cadditions
   from (select commithash, committerts, sum(addition) as additions
         from (select commithash, committerts
                from ref_commit_list natural join commits
                where refname=?
                and commithash not in
                  (select commithash from ref_commit_list
	           where refname=?))
	     natural join commitfilestat
	     group by commithash)
  window rowwindow as (order by committerts)
`, RefIn, RefNotIn)
	if err != nil {
		return nil, fmt.Errorf("Getting cumulative commits: %v", err)
	}

	logrus.Infof("Found %d data points", len(vals))

	xys := make(plotter.XYs, len(vals))
	if len(vals) != 0 {
		baseTime := vals[0].CommitterTs

		for i := range vals {
			xys[i].X = durationToDays(vals[i].CommitterTs.Sub(baseTime.Time))
			xys[i].Y = float64(vals[i].CAdditions)
		}
	}

	return xys, nil
}
