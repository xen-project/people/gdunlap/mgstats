/*
Copyright © 2024 George Dunlap, Cloud Software Group <george.dunlap@cloud.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/jmoiron/sqlx"
	"github.com/spf13/cobra"
	"gitlab.com/martyros/sqlutil/txutil"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xeninfo"
	"gitlab.com/xen-project/people/gdunlap/mgstats/htmlstat"
	"gitlab.com/xen-project/people/gdunlap/mgstats/mlstats"
)

// sunburstCmd represents the sunburst command
var sunburstCmd = &cobra.Command{
	Use:   "sunburst",
	Short: "Create a sunburst chart for mailing list stats",
	Long: `For now, sunburst will always create a sunburst chart for non-bot
emails to xen-devel since the last release.  Future work:  Add an option
to create one for the last release if it doesn't exist.`,
	Run: func(cmd *cobra.Command, args []string) {
		if idmapFile == "" {
			log.Fatalf("No idmap file specified.  Please use --idmap or set IDMAPDB.")
		}

		if lmdbFile == "" {
			log.Fatalf("No lmdb file specified.  Please use --lmdb or set LMDB.")
		}

		vs, err := xeninfo.VersionInfoList()
		if err != nil {
			log.Fatalf("Getting Xen versions: %v", err)
		}

		log.Printf("Most recent Xen version %v, released at %v", vs[0].XenVersion, vs[0].Time)

		cfg := mlstats.DbConfig{
			IdMapFile:    idmapFile,
			AnalysisFile: annotationsFile,
		}

		// Open lmdb file
		db, err := sqlx.Open("sqlite3", "file:"+lmdbFile+"?_fk=true&mode=rwc")
		if err != nil {
			log.Fatalf("Opening lmdb database: %v", err)
		}

		// Attach idmap, mlstats
		err = txutil.TxLoopDb(db, func(eq sqlx.Ext) error {
			return mlstats.AttachTx(eq, cfg)
		})
		if err != nil {
			log.Fatalf("mlstats.Attachtx(%v): %v", cfg, err)
		}

		tagname := "devel"

		log.Printf("Tagging non-bot messages since the %v release", vs[0].XenVersion)
		if err := mlstats.TagMessages(db, mlstats.MessageSelection{
			Tagname:     tagname,
			StartDate:   vs[0].Time.Format("2006-03-15"),
			ExcludeTags: []string{"bot"},
		}); err != nil {
			log.Fatalf("Tagging messages: %v", err)
		}

		log.Printf("Annotating the tag")
		if err := mlstats.AnnotateTaggedMessages(db, mlstats.AnnoteTaggedArgs{Tagname: tagname}); err != nil {
			log.Fatalf("Annotating messages: %v", err)
		}

		log.Printf("Querying company, people entities")
		ents, err := mlstats.GetEntryStats(db, mlstats.EntityStatArgs{
			Tagname: tagname,
			Entity:  mlstats.EntityPersonCompany,
		})
		if err != nil {
			log.Fatalf("GetEntryStats: %v", err)
		}

		sbdata, err := htmlstat.SunburstImportData(ents, func(e mlstats.EntityStat) ([]string, float64, error) {
			return []string{"xen-devel", e.CompanyLabel, e.PersonLabel}, float64(e.Count), nil
		})
		if err != nil {
			log.Fatalf("Converting to sunburst: %v", err)
		}

		ofname, err := cmd.Flags().GetString("output")
		if err != nil {
			log.Fatalf("Getting output argument: %v", err)
		}
		if ofname == "" {
			ofname = fmt.Sprintf("xen-%s.html", tagname)
		}

		f, err := os.Create(ofname)
		if err != nil {
			log.Fatalf("Creating output file %s: %v", ofname, err)
		}

		err = htmlstat.SunburstBasicPage(f, sbdata)
		if err != nil {
			log.Fatalf("Writing basic sunburst data: %v", err)
		}
	},
}

func init() {
	mlCmd.AddCommand(sunburstCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// starbustCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	sunburstCmd.Flags().StringP("output", "o", "", "Output to file (default xen-$VERSION)")
}
