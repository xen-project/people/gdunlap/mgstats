/*
Copyright © 2024 George Dunlap, Cloud Software Group <george.dunlap@cloud.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/xen-project/people/gdunlap/mgstats/gitdb"
	"gitlab.com/xen-project/people/gdunlap/mgstats/idmap"
	"gitlab.com/xen-project/people/gdunlap/mgstats/xenstats/reports"
)

// sunburstCmd represents the sunburst command
var gitContributionsCmd = &cobra.Command{
	Use:   "git-contributions",
	Short: "Create a CSV of git contribution statistics",
	Run: func(cmd *cobra.Command, args []string) {
		if idmapFile == "" {
			log.Fatalf("No idmap file specified.  Please use --idmap or set IDMAPDB.")
		}

		if gitDbFile == "" {
			log.Fatalf("No gitdb file specified.  Please use --gitdb or set LMDB.")
		}

		ofname, err := cmd.Flags().GetString("output")
		if err != nil {
			log.Fatalf("Getting output argument: %v", err)
		}
		if ofname == "" {
			ofname = fmt.Sprintf("gitstats-%s.csv", time.Now().Format("2006-01-02"))
		}

		gdb, err := gitdb.Open(gitDbFile)
		if err != nil {
			log.Fatalf("Opening gitdb file %s: %v", gitDbFile, err)
		}

		err = idmap.Attach(gdb.DB, idmap.DbConfig{Filename: idmapFile})
		if err != nil {
			log.Fatalf("Attaching idmapfile %s: %v", idmapFile, err)
		}

		gc, err := reports.GitContributionReport(gdb, reports.GitContributionReportArgs{
			PeopleLimit: 10, CompaniesLimit: 5,
		})
		if err != nil {
			log.Fatalf("Getting contribution report: %v", err)
			return
		}

		f, err := os.Create(ofname)
		if err != nil {
			log.Fatalf("Creating output file %s: %v", ofname, err)
		}

		cw := csv.NewWriter(f)

		writeOrDie := func(r []string) {
			if err := cw.Write(r); err != nil {
				log.Fatalf("Writing CSV: %v", err)
			}
		}

		// Marshall gc into CSV
		writeOrDie(append([]string{""}, gc.Colnames...))

		marshalRow := func(gcc *reports.GitContributionsRow) {
			r := []string{gcc.Label}
			for _, v := range gcc.Cols {
				r = append(r, fmt.Sprintf("%d", v))
			}
			writeOrDie(r)
		}

		marshalRowGroup := func(gcrg *reports.GitContributionsRowGroup) {
			writeOrDie([]string{gcrg.Label})
			for _, gcr := range gcrg.Tags {
				marshalRow(gcr)
			}
		}

		writeOrDie([]string{"*Community Totals*"})
		marshalRowGroup(gc.Total)

		writeOrDie([]string{"*People*"})
		for _, gcrg := range gc.People {
			marshalRowGroup(gcrg)
		}

		writeOrDie([]string{"*Companies*"})
		for _, gcrg := range gc.Companies {
			marshalRowGroup(gcrg)
		}

		cw.Flush()
		if cw.Error() != nil {
			log.Fatalf("Flushing CSV: %v", err)
		}
	},
}

func init() {
	reportCmd.AddCommand(gitContributionsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// starbustCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	gitContributionsCmd.Flags().StringP("output", "o", "", "Output to file (default gitstats-$FROM-$TO.csv)")
}
