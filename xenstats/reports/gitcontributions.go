package reports

import (
	"fmt"
	"time"

	"github.com/doug-martin/goqu/v9"
	"gitlab.com/xen-project/people/gdunlap/mgstats/gitdb"
	"gitlab.com/xen-project/people/gdunlap/mgstats/idmap"
	"gitlab.com/xen-project/people/gdunlap/mgstats/utils"
	"golang.org/x/exp/slices"

	"gitlab.com/martyros/sqlutil/types/timedb"
)

type GitContributionsRow struct {
	Label string
	Cols  []int
}

type GitContributionsRowGroup struct {
	Label string
	Tags  []*GitContributionsRow
	Total *GitContributionsRow
}

// GitContributions contains a matrix of contributions.  Row "groups" are
// People, Companies, or "Total"; columns may be "Total" and bins based on date.
// The name of the columns is given in Colname.
type GitContributions struct {
	Total         *GitContributionsRowGroup
	People        []*GitContributionsRowGroup
	Companies     []*GitContributionsRowGroup
	Colnames      []string
	Tagnames      []string
	OtherTagnames []string // Tags binned in "Other"
}

func (gc *GitContributions) NewRow(label string) *GitContributionsRowGroup {
	gcl := &GitContributionsRowGroup{
		Label: label,
		Tags:  make([]*GitContributionsRow, len(gc.Tagnames)),
		Total: &GitContributionsRow{
			Label: "Total",
			Cols:  make([]int, len(gc.Colnames)),
		}}
	for i, tagname := range gc.Tagnames {
		gcl.Tags[i] = &GitContributionsRow{
			Label: tagname,
			Cols:  make([]int, len(gc.Colnames)),
		}
	}
	return gcl
}

// FindRowGroup will look through the slice for a GitContributionsLabel
// matching the label; if none is found, it will add an element to the slice and
// return it.
func (gc *GitContributions) FindRowGroup(label string, list *[]*GitContributionsRowGroup) *GitContributionsRowGroup {
	if *list != nil {
		idx := slices.IndexFunc(*list, func(gcl *GitContributionsRowGroup) bool { return gcl.Label == label })
		if idx >= 0 {
			return (*list)[idx]
		}
	}
	gcl := gc.NewRow(label)
	*list = append(*list, gcl)
	return gcl
}

func timeToBinLabel(ts time.Time) string {
	return ts.Format("2006-01")
}

func (gc *GitContributions) FindRow(label string, gcrg *GitContributionsRowGroup) *GitContributionsRow {
	for i, tagname := range gc.Tagnames {
		if tagname == label {
			return gcrg.Tags[i]
		}
	}
	return nil
}

func (gc *GitContributions) FindColumn(label string, gcr *GitContributionsRow) *int {
	for i, colname := range gc.Colnames {
		if colname == label {
			return &gcr.Cols[i]
		}
	}
	return nil
}

type GitContributionReportArgs struct {
	Refname        string
	StartDate      string
	PeopleLimit    int
	CompaniesLimit int
	Tagnames       []string
}

const (
	ColTotal = "Total"
	RowTotal = "Total"
	RowOther = "Other"
	TagOther = "Other tags"
)

func GitContributionReport(gdb *gitdb.GitDB, args GitContributionReportArgs) (*GitContributions, error) {
	gc := &GitContributions{
		Colnames: []string{ColTotal},
		Tagnames: []string{TagOther},
	}

	if args.Refname == "" {
		args.Refname = "staging"
	}

	// If no start date is given, start at the beginning of the month one year
	// ago
	if args.StartDate == "" {
		now := time.Now()
		yearAligned := time.Date(now.Year()-1, now.Month(), 1, 0, 0, 0, 0, time.UTC)
		args.StartDate = yearAligned.Format("2006-01-02")
	}

	if args.Tagnames == nil {
		// TODO: Consider doing a query of the DB to get all tags first
		gc.Tagnames = append(gc.Tagnames,
			[]string{"Signed-off-by", "Reviewed-by", "Acked-by"}...)
	} else {
		gc.Tagnames = append(gc.Tagnames, args.Tagnames...)
	}

	// Set up "bins" by month
	{
		start, err := time.Parse("2006-01-02", args.StartDate)
		if err != nil {
			return nil, fmt.Errorf("Parsing Start date %s: %v", args.StartDate, err)
		}
		// Clip the start date to the beginning of the month

		end := time.Now()

		for start = time.Date(start.Year(), start.Month(), 1, 0, 0, 0, 0, time.UTC); start.Before(end); start = time.Date(start.Year(), start.Month()+1, 1, 0, 0, 0, 0, time.UTC) {
			label := timeToBinLabel(start)
			gc.Colnames = append(gc.Colnames, label)
		}
	}

	gc.Total = gc.NewRow(RowTotal)

	// Query all tags found in commit messages on the given branch in the given
	// time range
	q := gitdb.QueryCommitSelection(gitdb.CommitSelection{
		Refname:   args.Refname,
		StartDate: args.StartDate,
	})

	q = gitdb.QueryAnnotateSignatures(q)

	q = goqu.From(q).
		Select(
			gitdb.MTagname,
			goqu.C("committerts"),
			idmap.CPersonLabel,
			idmap.CCompanyLabel,
		)

	res := []struct {
		Tagname      string
		CommitterTs  timedb.Time
		Personlabel  string
		Companylabel string
	}{}

	if err := utils.QuerySelect(gdb, &res, q); err != nil {
		return nil, fmt.Errorf("Executing query: %w", err)
	}

	// Now aggregate tags by {total, people company} x {total, date bin}

	addContributionCol := func(gc *GitContributions, gcrg *GitContributionsRowGroup,
		collabel string, tagidx int) {
		(*gc.FindColumn(collabel, gcrg.Tags[tagidx]))++
		(*gc.FindColumn(collabel, gcrg.Total))++
	}

	addContributions := func(gc *GitContributions, collabel, personlabel, companylabel string, tagidx int) {
		addContributionCol(gc, gc.Total, collabel, tagidx)
		addContributionCol(gc, gc.FindRowGroup(personlabel, &gc.People), collabel, tagidx)
		addContributionCol(gc, gc.FindRowGroup(companylabel, &gc.Companies), collabel, tagidx)
	}

	for _, r := range res {
		tagidx := slices.Index(gc.Tagnames, r.Tagname)
		if tagidx < 0 {
			tagidx = 0
			if !slices.Contains(gc.OtherTagnames, r.Tagname) {
				gc.OtherTagnames = append(gc.OtherTagnames, r.Tagname)
			}
		}
		addContributions(gc, RowTotal, r.Personlabel, r.Companylabel, tagidx)
		addContributions(gc,
			timeToBinLabel(r.CommitterTs.Time),
			r.Personlabel, r.Companylabel, tagidx)
	}

	// Now sort aggregate high to low by total tags
	slices.SortFunc(gc.People, func(a, b *GitContributionsRowGroup) int {
		return b.Total.Cols[0] - a.Total.Cols[0]
	})

	slices.SortFunc(gc.Companies, func(a, b *GitContributionsRowGroup) int {
		return b.Total.Cols[0] - a.Total.Cols[0]
	})

	// And finally, apply limit functions
	limit := func(limit int, list *[]*GitContributionsRowGroup) {
		if limit > 0 && len(*list) > limit {
			otherSlice := (*list)[limit:]
			othercl := gc.NewRow(RowOther)

			for _, gcrg := range otherSlice {
				for tagidx, gcr := range gcrg.Tags {
					for colidx, val := range gcr.Cols {
						othercl.Tags[tagidx].Cols[colidx] += val
					}
				}
				for colidx, val := range gcrg.Total.Cols {
					othercl.Total.Cols[colidx] += val
				}
			}
			*list = append((*list)[:limit], othercl)
		}
	}

	limit(args.PeopleLimit, &gc.People)

	limit(args.CompaniesLimit, &gc.Companies)

	return gc, nil
}
