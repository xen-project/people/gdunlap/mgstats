package reports_test

import (
	"os"
	"testing"

	"gitlab.com/xen-project/people/gdunlap/mgstats/gitdb"
	"gitlab.com/xen-project/people/gdunlap/mgstats/idmap"
	"gitlab.com/xen-project/people/gdunlap/mgstats/xenstats/reports"
)

const (
	EnvTestGitDbFile = "GOTEST_GITDB"
	EnvTestIdmapFile = "GOTEST_IDMAP"
)

func TestAnnotationQuery(t *testing.T) {
	gitdbfile := os.Getenv(EnvTestGitDbFile)
	if gitdbfile == "" {
		t.Skipf("%s not defined, skipping", EnvTestGitDbFile)
	}

	idmapfile := os.Getenv(EnvTestIdmapFile)
	if idmapfile == "" {
		t.Skipf("%s not defined, skipping", EnvTestIdmapFile)
	}

	gdb, err := gitdb.Open(gitdbfile)
	if err != nil {
		t.Errorf("Opening gitdb file %s: %v", gitdbfile, err)
	}

	err = idmap.Attach(gdb.DB, idmap.DbConfig{Filename: idmapfile})
	if err != nil {
		t.Errorf("Attaching idmapfile %s: %v", idmapfile, err)
	}

	gc, err := reports.GitContributionReport(gdb, reports.GitContributionReportArgs{
		PeopleLimit: 10, CompaniesLimit: 5,
	})
	if err != nil {
		t.Errorf("Getting contribution report: %v", err)
		return
	}

	// Check that all rows have the same number of columns
	if len(gc.Total.Tags) != len(gc.Tagnames) {
		t.Errorf("ERROR: len(Total.Cols) %d, len(Colnames) %d!",
			len(gc.Total.Tags), len(gc.Tagnames))
	}
	for _, gcrg := range gc.People {
		if len(gcrg.Tags) != len(gc.Total.Tags) {
			t.Errorf("ERROR: People label %s: Expected %d cols, got %d!",
				gcrg.Label, len(gc.Total.Tags), len(gcrg.Tags))
		}
	}

	t.Logf("People:")
	for _, p := range gc.People {
		t.Logf("  %s (%d total):", p.Label, gc.FindColumn(reports.ColTotal, p.Total))
		for _, gcr := range p.Tags {
			t.Logf("    %15s: %3d", gcr.Label, gcr.Cols[0])
		}
	}

	t.Logf("Companies:")
	for _, p := range gc.Companies {
		t.Logf("  %s (%d total):", p.Label, gc.FindColumn(reports.ColTotal, p.Total))
		for _, gcr := range p.Tags {
			t.Logf("    %15s: %3d", gcr.Label, gcr.Cols[0])
		}
	}

	t.Logf("'Other' tagnames: %v", gc.OtherTagnames)
}
